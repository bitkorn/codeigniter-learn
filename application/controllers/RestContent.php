<?php

/**
 * REST Controller uses https://github.com/chriskacerguis/codeigniter-restserver
 */
//require APPPATH . '/libraries/Restserver/REST_Controller.php'; // not required with /application/vendor/autoload.php

use Restserver\REST_Controller;

/**
 *
 * @author Torsten Brieskorn
 */
class RestContent extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('cmsModel');
        $this->load->helper('url_helper');
    }

//    public function content()
    public function content_get()
    {
        $id = $this->get('id');

//        log_message('debug', '#################### content_get() $id: ' . $id);

        if ($id === NULL) {
            $content = $this->cmsModel->getContent();
            if ($content) {
                // Set the response and exit
                $this->response($content, \Restserver\REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No content were found'
                        ], \Restserver\REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $id = (int) $id;

            // Validate the id.
            if ($id <= 0) {
                // Invalid id, set the response and exit.
                $this->response(NULL, \Restserver\REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }

            $content = $this->cmsModel->getContentById($id);

            if (!empty($content)) {
                $this->set_response($content, \Restserver\REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            } else {
                $this->set_response([
                    'status' => FALSE,
                    'message' => 'Content could not be found'
                        ], \Restserver\REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

}
