<?php

/**
 *
 * @author Torsten Brieskorn
 */
class ContentAdmin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('cmsModel');
        $this->load->helper('url_helper');
    }

    /**
     * e.g. http://cilearn.local/contentAdmin/create
     * 
     * @param string $sefurl
     */
    public function create()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('ci_cms_content_title', 'Title', 'required');
        $this->form_validation->set_rules('ci_cms_content_content', 'Content', 'required');

        if ($this->form_validation->run() === FALSE) {
            $data['title'] = 'Create a cms item';
            $this->load->view('cms/admin/templates/header', $data);
            $this->load->view('cms/admin/body/content');
            $this->load->view('cms/admin/templates/footer');
        } else {
            $id = $this->cmsModel->saveContent();
//            log_message('debug', 'saveContent(): ' . $id);
            if (empty($id)) {
                /**
                 * @todo Show Error Message to user
                 */
            } else {
                redirect('contentAdmin/edit/' . $id);
            }
        }
    }

    /**
     * e.g. http://cilearn.local/index.php/contentAdmin/edit/1
     * 
     * @param int $id
     */
    public function edit($id = 0)
    {
        $content = $this->cmsModel->getContentById($id);
        if (empty($content)) {
            /**
             * @todo Show Error Message to user
             */
        }
        if ($this->input->method() == 'get') {
            $data['content'] = $content;
        } else {
            $data['content'] = $this->input->post(null, true);
        }
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('ci_cms_content_title', 'Title', 'required');
        $this->form_validation->set_rules('ci_cms_content_content', 'Content', 'required');

        $data['title'] = 'Edit cms item';
        $this->load->view('cms/admin/templates/header', $data);
        $this->load->view('cms/admin/body/contentEdit');
        $this->load->view('cms/admin/templates/footer');

        if ($this->form_validation->run() === FALSE) {
            
        } else {
            $count = $this->cmsModel->editContent();
            log_message('debug', 'editContent(): ' . $count);
            if (1 > $count) {
                /**
                 * @todo Show Error Message to user
                 */
            }
        }
    }

}
