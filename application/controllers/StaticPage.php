<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author Torsten Brieskorn
 */
class StaticPage extends CI_Controller
{

    /**
     * Ohne routes:
     * http://cilearn.local/index.php/StaticPage/hellostatic
     * 
     */
    public function helloStatic()
    {
        $this->load->view('static/hello');
    }

    /**
     * Ohne routes & mod_rewrite:
     * http://cilearn.local/index.php/StaticPage/view
     * oder (kleines 's' ...sonst case sensitive)
     * http://cilearn.local/index.php/staticPage/view
     * http://cilearn.local/index.php/StaticPage/view/about
     * 
     * Mit mod_rewrite
     * http://cilearn.local/staticPage/view/about
     * 
     * @param type $page
     */
    public function view($page = 'home')
    {
        if (!file_exists(APPPATH . 'views/static/body/' . $page . '.php')) {
            show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter

        $this->load->view('static/templates/header', $data);
        $this->load->view('static/body/' . $page, $data);
        $this->load->view('static/templates/footer', $data);
    }

}
