<?php

//namespace Ajax;

defined('BASEPATH') OR exit('No direct script access allowed');

class WelcomeAjax extends CI_Controller
{

    public function someajax($ajaxparam = '')
    {
        $this->load->view('static/ajax', ['ajaxparam' => $ajaxparam]);
    }

}
