<?php

/**
 *
 * @author Torsten Brieskorn
 */
class Content extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('cmsModel');
        $this->load->helper('url_helper');
    }

    /**
     * e.g. http://cilearn.local/content/view/some-sefurl
     * and with route $route['cms/(:any)'] = 'content/view/$1':
     * http://cilearn.local/cms/some-sefurl
     * 
     * @param string $sefurl
     */
    public function view($sefurl = '')
    {
        $content = $this->cmsModel->getContentBySefurl($sefurl);
        if(empty($content)) {
            show_404();
        }
        $data['title'] = $content['ci_cms_content_title'];
        $data['content'] = $content['ci_cms_content_content'];

        $this->load->view('cms/frontend/templates/header', $data);
        $this->load->view('cms/frontend/body/cms', $data);
        $this->load->view('cms/frontend/templates/footer', $data);
    }

}
