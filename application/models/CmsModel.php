<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of CmsModel
 *
 * @author Torsten Brieskorn
 */
class CmsModel extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }
    
    public function getContent()
    {
        $query = $this->db->get('ci_cms_content');
        return $query->result_array();
    }

    /**
     * 
     * @param int $id
     * @return array
     */
    public function getContentById($id = 0)
    {
        if ($id == 0) {
            return [];
        }

        $query = $this->db->get_where('ci_cms_content', array('ci_cms_content_id' => $id));
        return $query->row_array();
    }

    /**
     * 
     * @param string $sefurl
     * @return array
     */
    public function getContentBySefurl($sefurl = '')
    {
        if ($sefurl == '') {
            $query = $this->db->get('ci_cms_content');
            return $query->result_array();
        }

        $query = $this->db->get_where('ci_cms_content', array('ci_cms_content_sefurl' => $sefurl));
        return $query->row_array();
    }

    public function saveContent()
    {
        $this->load->helper('url'); // to use function url_title()

        $cmsTitle = $this->input->post('ci_cms_content_title');
        $cmsSefurl = url_title($cmsTitle, '-', true);

        $data = array(
            'ci_cms_content_sefurl' => $cmsSefurl,
            'ci_cms_content_title' => $cmsTitle,
            'ci_cms_content_content' => $this->input->post('ci_cms_content_content')
        );

        if ($this->db->insert('ci_cms_content', $data) == 1) {
            return $this->db->insert_id();
        } else {
            return 0;
        }
    }

    public function editContent()
    {
        $this->load->helper('url'); // to use function url_title()

        $cmsTitle = $this->input->post('ci_cms_content_title');
        $cmsSefurl = url_title($cmsTitle, '-', true);

        $data = array(
            'ci_cms_content_sefurl' => $cmsSefurl,
            'ci_cms_content_title' => $cmsTitle,
            'ci_cms_content_content' => $this->input->post('ci_cms_content_content')
        );
        $this->db->set($data);
        $this->db->where('ci_cms_content_id = ' . $this->input->post('ci_cms_content_id'));
        return $this->db->update('ci_cms_content');
    }

}
